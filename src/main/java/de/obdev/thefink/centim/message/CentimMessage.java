package de.obdev.thefink.centim.message;

import java.util.UUID;

/**
 * Created by obdev on 28.07.2016.
 */
public class CentimMessage {

    UUID messageId;
    UUID receiverId;
    UUID senderId;

    public CentimMessage() {
        this.messageId = UUID.randomUUID();
    }



    public CentimMessage(UUID messageId, UUID receiverId, UUID senderId) {
        this.messageId = messageId;
        this.receiverId = receiverId;
        this.senderId = senderId;
    }

    public CentimMessage(UUID receiverId, UUID senderId) {
        this.messageId = UUID.randomUUID();
        this.receiverId = receiverId;
        this.senderId = senderId;
    }

    public UUID getMessageId() {
        return messageId;
    }

    public UUID getReceiverId() {
        return receiverId;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public void setMessageId(UUID messageId) {
        this.messageId = messageId;
    }

    public void setReceiverId(UUID receiverId) {
        this.receiverId = receiverId;
    }

    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }

    @Override
    public String toString() {
        return "CentimMessage{" +
                "messageId=" + messageId +
                ", receiverId=" + receiverId +
                ", senderId=" + senderId +
                '}';
    }
}
