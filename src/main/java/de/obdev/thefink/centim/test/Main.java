package de.obdev.thefink.centim.test;

import java.io.IOException;

/**
 * Created by obdev on 28.07.2016.
 */
public class Main {

    public static void main(String[] args)  {


        if (args[0].toLowerCase().endsWith("client")) {
            try {
                TestClient.main(args);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (args[0].toLowerCase().endsWith("server")) {
            TestServer.main(args);
        }



    }

}
