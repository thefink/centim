package de.obdev.thefink.centim.test;

import com.esotericsoftware.kryo.serializers.JavaSerializer;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.obdev.thefink.centim.connection.CConnection;
import de.obdev.thefink.centim.connection.confirmation.CConfirmation;
import de.obdev.thefink.centim.connection.confirmation.ConfirmationOfficer;
import de.obdev.thefink.centim.connection.object.CentimObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by obdev on 29.07.2016.
 */
public class TestClient {

    public static void main(String[] args) throws IOException {

        Logger logger = LogManager.getLogger(TestServer.class);

        Client client = new Client();
        client.start();
        client.getKryo().register(TestMessage.class);
        client.getKryo().register(UUID.class);
        client.getKryo().register(CentimObject.class);
        client.getKryo().register(CConfirmation.class);
        client.getKryo().register(ArrayList.class, new JavaSerializer());

        client.getKryo().setInstantiatorStrategy(new StdInstantiatorStrategy());
        client.connect(
                5000,
                "localhost",
                Integer.valueOf(args[1]),
                Integer.valueOf(args[2])
        );

        CConnection cConnection = CConnection.get(client);

        client.addListener(new ConfirmationOfficer());

        client.addListener(
                new Listener() {
                    @Override
                    public void received(Connection connection, Object o) {
                        if (o instanceof CentimObject) {
                            CentimObject object = ((CentimObject) o);

                            if (object.get() instanceof UUID) {
                                //logger.debug("Recived UUID, {}", object.get().toString());
                                cConnection.sendFS(new CentimObject(object.get()));
                            }

                        }
                    }
                }
        );






    }

}
