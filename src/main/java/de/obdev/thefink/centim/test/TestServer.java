package de.obdev.thefink.centim.test;

import com.esotericsoftware.kryo.serializers.JavaSerializer;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import de.obdev.thefink.centim.connection.CConnection;
import de.obdev.thefink.centim.connection.confirmation.CConfirmation;
import de.obdev.thefink.centim.connection.confirmation.ConfirmationOfficer;
import de.obdev.thefink.centim.connection.object.CentimObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by obdev on 29.07.2016.
 */
public class TestServer {

    static List<String> sent = new ArrayList<>();
    static List<String> received = new ArrayList<>();

    static CConnection cConnection;

    public static void main(String[] args) {

        Logger logger = LogManager.getLogger(TestServer.class);

        Server server = new Server();
        server.start();


        try {
            server.bind(Integer.valueOf(args[1]), Integer.valueOf(args[2]));
            logger.debug("Binding bound");
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.getKryo().register(TestMessage.class);
        server.getKryo().register(UUID.class);
        server.getKryo().register(CentimObject.class);
        server.getKryo().register(CConfirmation.class);
        server.getKryo().register(ArrayList.class, new JavaSerializer());

        server.getKryo().setInstantiatorStrategy(new StdInstantiatorStrategy());

        server.addListener(
                new ConfirmationOfficer()
        );



        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    CentimObject object = new CentimObject(
                            new Date().toString()
                    );

                    sent.add(object.getId());

                    cConnection.sendFS(
                      object
                    );

                    logger.info("Sent: " + sent.size());
                    logger.info("Received: " + received.size());

                }
            }
        });



        server.addListener(
                new Listener() {
                    @Override
                    public void disconnected(Connection connection) {
                        logger.debug("Client " + connection.getID() + " disconnected");
                    }

                    @Override
                    public void connected(Connection connection) {

                        cConnection = CConnection.get(connection);
                        thread.start();

                    }

                    @Override
                    public void received(Connection connection, Object o) {
                        if (o instanceof CConfirmation) {
                            CConfirmation confirmation = (CConfirmation) o;
                            received.addAll(confirmation.getIds());
                        }
                    }

                    @Override
                    public void idle(Connection connection) {
                        System.out.println(connection.getID() + "is idle");
                    }
                }
        );
    }

}
