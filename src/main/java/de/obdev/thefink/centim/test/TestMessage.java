package de.obdev.thefink.centim.test;

import de.obdev.thefink.centim.message.CentimMessage;

import java.util.UUID;

/**
 * Created by obdev on 28.07.2016.
 */
public class TestMessage extends CentimMessage {

    private String document;

    public TestMessage() {

    }

    public TestMessage(UUID messageId, UUID receiverId, UUID senderId, String document) {
        super(messageId, receiverId, senderId);
        this.document = document;
    }

    public String getDocument() {
        return document;
    }
}
