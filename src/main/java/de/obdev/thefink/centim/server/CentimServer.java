package de.obdev.thefink.centim.server;

import com.esotericsoftware.kryo.serializers.JavaSerializer;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Serialization;
import com.esotericsoftware.kryonet.Server;
import de.obdev.thefink.centim.connection.CConnection;
import de.obdev.thefink.centim.connection.confirmation.CConfirmation;
import de.obdev.thefink.centim.connection.confirmation.ConfirmationOfficer;
import de.obdev.thefink.centim.connection.listener.CentimListener;
import de.obdev.thefink.centim.connection.object.CentimObject;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by obdev on 30.07.2016.
 */
public class CentimServer extends Server {

    List<Listener> listeners = new ArrayList<>();

    public CentimServer() {
        super();
        this.init();
    }

    public CentimServer(int i, int i1) {
        super(i, i1);
        this.init();
    }

    public CentimServer(int i, int i1, Serialization serialization) {
        super(i, i1, serialization);
        this.init();
    }

    private void init() {
        this.getKryo().register(CentimObject.class);
        this.getKryo().register(UUID.class);
        this.getKryo().register(CConfirmation.class);
        this.getKryo().register(ArrayList.class, new JavaSerializer());
        this.addListener(new ConfirmationOfficer());

        super.addListener(
                new Listener() {
                    @Override
                    public void connected(Connection connection) {
                        for (Listener listener:listeners) {
                            if (listener instanceof CentimListener) {
                                listener.connected(connection);
                                ((CentimListener) listener).connected(CConnection.get(connection));
                            } else {
                                listener.connected(connection);
                            }
                        }
                    }

                    @Override
                    public void disconnected(Connection connection) {
                        for (Listener listener:listeners) {
                            if (listener instanceof CentimListener) {
                                listener.disconnected(connection);
                                ((CentimListener) listener).disconnected(CConnection.get(connection));
                            } else {
                                listener.disconnected(connection);
                            }
                        }
                    }

                    @Override
                    public void received(Connection connection, Object o) {
                        for (Listener listener:listeners) {
                            if (listener instanceof CentimListener) {
                                listener.received(connection, o);
                                if (o instanceof CentimObject) {
                                    ((CentimListener) listener).received(
                                            CConnection.get(connection),
                                            ((CentimObject) o).get()
                                    );
                                }
                            } else {
                                listener.received(connection, o);
                            }
                        }
                    }

                    @Override
                    public void idle(Connection connection) {

                    }
                }
        );

    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        super.stop();
        for (Listener listener:listeners) {
            if (listener instanceof Closeable) {
                try {
                    ((Closeable) listener).close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        listeners.clear();
    }

    @Override
    public void addListener(Listener listener) {
        this.listeners.add(listener);
    }
}
