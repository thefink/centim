package de.obdev.thefink.centim.connection.object;

import java.util.UUID;

/**
 * Created by obdev on 30.07.2016.
 */
public class CentimObject extends Object {

    Object object;
    String id;

    public CentimObject(Object object) {
        this.object = object;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public Object get() {
        return object;
    }


}
