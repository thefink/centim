package de.obdev.thefink.centim.connection;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.obdev.thefink.centim.connection.confirmation.CConfirmation;
import de.obdev.thefink.centim.connection.object.CentimObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.*;

/**
 * Created by obdev on 30.07.2016.
 */
public class CConnection implements Runnable, Closeable {

    static Logger logger = LogManager.getLogger(CConnection.class);

    public static Map<Connection, CConnection> connections =Collections.synchronizedMap(new HashMap<>());

    public static Integer retries = 3;
    public static Integer maxRetries = 5;
    public static Integer timeout = 10000;

    public static Integer udpSize = 100;
    public static Integer tcpSize = 100;


    Map<String, CentimObject> udpObjs = Collections.synchronizedMap(new HashMap<>());
    Map<String, CentimObject> tcpObjs = Collections.synchronizedMap(new HashMap<>());


    CMonitoringOfficer officer;

    Connection connection;

    private CConnection(Connection connection) {
        this.connection = connection;
        this.connection.addListener(
                new Listener() {
                    @Override
                    public void received(Connection connection, Object o) {
                        if (o instanceof CConfirmation) {
                            CConfirmation confirmation = ((CConfirmation) o);
                            logger.debug("Received confirmation, {}", confirmation.getIds().size());
                            for (String id:confirmation.getIds()) {

                                officer.notifyConfirm(id);
                            }
                        } else if (o instanceof CentimObject) {
                            logger.debug("Received object, {}", ((CentimObject) o).getId());
                        }
                    }
                }
        );

        officer = new CMonitoringOfficer(this, retries, maxRetries, timeout);

        Thread thread = new Thread(this);
        thread.start();
        thread.setName("CConnection-" + connection.getID());

    }

    public static CConnection get(Connection connection) {
        if (connections.containsKey(connection)) {
            logger.debug("Loading CConnection");
            return connections.get(connection);
        } else {
            logger.debug("Creating new CConnection");
            CConnection cConnection = new CConnection(connection);
            connections.put(connection, cConnection);
            return cConnection;
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public Boolean isRunning() {
        return this.connection.isConnected();
    }

    public void sendFS(CentimObject object) {
        this.udpObjs.put(object.getId(), object);
    }



    public void sendS(CentimObject object) {
        this.tcpObjs.put(object.getId(), object);
    }


    @Override
    public void close() {
        connection.close();
        connections.remove(this.getConnection());
    }


    @Override
    public void run() {
        while (this.connection.isConnected()) {


            //Set<String> udpIds = new HashSet<>(this.udpObjs.keySet());
            List<String> udpIds = new ArrayList<>(this.udpObjs.keySet());

            Integer pos = 0;
            while (pos < udpSize && pos < udpIds.size()) {
                String id = udpIds.get(pos);
                if (this.udpObjs.containsKey(id)) {
                    CentimObject object = this.udpObjs.get(id);
                    logger.debug("Sending object via UDP, {}", object.getId());
                    this.connection.sendUDP(object);
                    this.officer.notifySend(object);
                    this.udpObjs.remove(id);
                }
                pos++;
            }

            //Set<String> tcpIds = new HashSet<>(this.tcpObjs.keySet());

            List<String> tcpIds = new ArrayList<>(this.tcpObjs.keySet());

            pos = 0;

            while (pos < tcpSize && pos < tcpIds.size()) {
                String id = tcpIds.get(pos);
                if (this.tcpObjs.containsKey(id)) {
                    CentimObject object = this.tcpObjs.get(id);
                    logger.debug("Sending object via TCP, {}", object.getId());
                    this.connection.sendTCP(object);
                    this.officer.notifySend(object);
                    this.tcpObjs.remove(id);
                }
                pos++;
            }


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
