package de.obdev.thefink.centim.connection;

/**
 * Created by obdev on 30.07.2016.
 */
public interface CCInterface {

    public void sendFS(Object o);
    public void sendS(Object o);
}
