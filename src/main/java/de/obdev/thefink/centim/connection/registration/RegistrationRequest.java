package de.obdev.thefink.centim.connection.registration;

import de.obdev.thefink.centim.message.CentimMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by obdev on 29.07.2016.
 */
public class RegistrationRequest extends CentimMessage {

    List<String> classes = new ArrayList<>();

    public void addClass(Class cls) {
        classes.add(cls.getName());
    }

    

}
