package de.obdev.thefink.centim.connection;

import de.obdev.thefink.centim.connection.object.CentimObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by obdev on 30.07.2016.
 */
public class CMonitoringOfficer implements Runnable, Closeable {

    CConnection cConnection;

    Map<String, CentimObject> objects = new HashMap<>();

    Map<String, Integer> times = new ConcurrentHashMap<>();
    Map<String, Integer> tries = new ConcurrentHashMap<>();

    Logger logger = LogManager.getLogger(this.getClass());

    Boolean running = true;

    Integer tcpTries;
    Integer maxRetries;
    Integer timeout;


    public CMonitoringOfficer(CConnection cConnection, Integer retries, Integer maxRetries, Integer timeout) {

        logger.debug("CMonitoringOfficer created");

        this.cConnection = cConnection;
        this.tcpTries = retries;
        this.maxRetries = maxRetries;
        this.timeout = timeout;
        Thread thread = new Thread(this);
        thread.start();
        thread.setName("CMonitoringOfficer-" + cConnection.getConnection().getID());
    }

    @Override
    public void run() {


        while (cConnection.isRunning()) {

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }





            Iterator<String> iterator = this.times.keySet().iterator();
            while (iterator.hasNext()) {
                String id = iterator.next();
                if (this.times.containsKey(id)) {
                    Integer time = this.times.get(id);

                    if (time > timeout) {

                        this.send(id);


                    } else if (time >= 0) {

                        this.times.put(id, time + 10);

                    }
                }

            }


        }
    }

    private void send(String id) {

        Integer retries = tries.getOrDefault(id, 0);

        if (retries > this.maxRetries) {
            logger.debug("Aborting retry, {}", id);
            this.remove(id);
        } else if (retries < tcpTries) {
            if (objects.containsKey(id)) {
                this.times.put(id, -1);
                logger.info("Requesting retry via UDP, {}", id);
                cConnection.sendFS(objects.get(id));
            }
        } else {
            if (objects.containsKey(id)) {
                this.times.put(id, -1);
                logger.info("Requesting retry via TCP, {}", id);
                cConnection.sendS(objects.get(id));
            }
        }


    }

    private void remove(String id) {
        logger.debug("Removing object, {}", id);
        if (times.containsKey(id)) {
            times.remove(id);
        }
        if (objects.containsKey(id)) {
            objects.remove(id);
        }
        if (tries.containsKey(id)) {
            tries.remove(id);
        }

    }


    public void notifySend(CentimObject object) {
        times.put(object.getId(), 0);
        objects.put(object.getId(), object);

        tries.put(object.getId(), tries.getOrDefault(object.getId(), 0) + 1);


    }

    public void notifyConfirm(String id) {

        this.remove(id);

    }

    @Override
    public void close() throws IOException {
        this.running = false;
    }
}
