package de.obdev.thefink.centim.connection.confirmation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by obdev on 30.07.2016.
 */
public class CConfirmation {

    List<String> ids = new ArrayList<>();

    public CConfirmation() {

    }

    public CConfirmation(List<String> ids) {
        this.ids = ids;
    }

    public void addId(String id) {
        ids.add(id);
    }

    public List<String> getIds() {
        return ids;
    }
}
