package de.obdev.thefink.centim.connection.config;

import java.util.UUID;



/**
 * Created by obdev on 28.07.2016.
 */
public class ConnectionConfig {

    Integer timeout;
    String host;
    Integer tcpPort;
    Integer udpPort;
    UUID receiverId;

    public ConnectionConfig(Integer timeout, String host, Integer tcpPort, Integer udpPort, UUID receiverId) {
        this.timeout = timeout;
        this.host = host;
        this.tcpPort = tcpPort;
        this.udpPort = udpPort;
        this.receiverId = receiverId;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public String getHost() {
        return host;
    }

    public Integer getTcpPort() {
        return tcpPort;
    }

    public Integer getUdpPort() {
        return udpPort;
    }

    public UUID getReceiverId() {
        return receiverId;
    }
}
