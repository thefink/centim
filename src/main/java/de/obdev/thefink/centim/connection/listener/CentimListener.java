package de.obdev.thefink.centim.connection.listener;

import com.esotericsoftware.kryonet.Listener;
import de.obdev.thefink.centim.connection.CConnection;
import de.obdev.thefink.centim.connection.object.CentimObject;

/**
 * Created by obdev on 29.07.2016.
 */
public abstract class CentimListener extends Listener {

    public void connected(CConnection connection) {
    }

    public void disconnected(CConnection connection) {
    }

    public void received(CConnection connection, Object object) {
    }


}
