package de.obdev.thefink.centim.connection.confirmation;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimaps;
import de.obdev.thefink.centim.connection.object.CentimObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by obdev on 30.07.2016.
 */
public class ConfirmationOfficer extends Listener implements Runnable, Closeable {

    Logger logger = LogManager.getLogger(this.getClass());

    public static Integer timeout = 1000;

    ListMultimap<Connection, String> objects = Multimaps.synchronizedListMultimap(ArrayListMultimap.create());

    private Boolean running = true;

    public ConfirmationOfficer() {
        Thread thread = new Thread(this);
        thread.start();
        thread.setName("ConfirmationOfficer-" + Thread.currentThread().getId());
    }

    @Override
    public void received(Connection connection, Object o) {

        if (o instanceof CentimObject) {
            CentimObject object = ((CentimObject) o);

            logger.debug("Received object, {}", object.getId());

            this.objects.put(connection, object.getId());


        }
    }


    @Override
    public void run() {
        while (running) {

            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            Iterator<Connection> iterator = this.objects.keySet().iterator();
            while(iterator.hasNext()) {
                Connection connection = iterator.next();
                List<String> conformationList = new ArrayList<>(this.objects.get(connection));

                for (List<String> single: Lists.partition(conformationList, 20)) {
                    CConfirmation confirmation = new CConfirmation(
                            new ArrayList<>(single)
                    );
                    logger.debug("Sending confirmations, {}", single.size());

                    connection.sendUDP(
                            confirmation
                    );
                }

                for (String id:conformationList) {
                    this.objects.remove(connection, id);
                }
            }




        }
    }

    @Override
    public void close() throws IOException {
        this.running = false;
    }
}
